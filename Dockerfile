FROM centos

RUN yum -y install -y curl policycoreutils-python openssh-server postfix sudo

RUN curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.rpm.sh | sudo bash && \
    yum -y install gitlab-ce && EXTERNAL_URL="http://git.lab.com" 

COPY run.sh /tmp

CMD /tmp/run.sh

EXPOSE 80 443
